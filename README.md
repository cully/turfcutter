# CiviCRM Turfcutter

![An image of the turfcutter](images/turfcutter.png)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP
* CiviCRM
* cv
* npm
* latitude/longitude for your contacts. This extension does not geocode.

 Functional versions are unknown, as I am simply trying to resuscitate this from beyond the grave. I am using CiviCRM 5.44.0 and PHP 7.4.3

## Read before starting!!
I'm a paramedic. I have no business doing any sort of work in this extension. But I need it, and the creator of it has moved on. I'm doing what I can to polish it up a bit. It may be a polished turd, but hopefully it functions.

After you git/cv dl the package, it is **absolutely imperative** you run npm install. I spent a year mad that this extension didn't even generate a map because I missed that step. Don't be me.

Once you've done all the steps and you are to the point where you can see the map, come back to this next paragraph.

## Street Smarts

The contact import for this is done via groups, so whatever contacts you want to cut turf for must be broken down into groups in some way. Turfcutter does have the ability to do boolean functions in comparing groups. Once you've got a group identified and ready to be cut, click the button on the right that says 'Click here to import a group.' Here's where you'll select your group. Find your group, and just like that, your contacts are on the map, ready to be cut!

Select a shape on the left and use it to ensnare one walklist of candidates. A popup box appears listing the documented volunteers you have, as well as a text box for a group name. You now have three buttons.
* Draw Another Shape
    * This will close the popup and allow you to cut additional turfs, pausing action on the turf you just drew.
* Save Voters as Group
    * This will take the voters within that turf and assign them to a static group.
* Create and Assign Turf
    * This will assign the voters to the volunteer.

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd $(cv ev --out=pretty 'return (new CRM_Extension_System())->getDefaultContainer()->baseDir')
cv dl https://gitlab.com/cully/turfcutter/-/archive/master/turfcutter-master.zip
cd turfcutter
npm install
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd $(cv ev --out=pretty 'return (new CRM_Extension_System())->getDefaultContainer()->baseDir')
git clone https://gitlab.com/cully/turfcutter
cv en turfcutter
cd turfcutter
npm install
```

## Usage

Currently one of the most time consuming tasks for a political campaign is to be able to "cut turf". This is the process by which a staffer or campaign manager takes a large list (such as an entire precinct of 500 doors) and divides it into palatable lists that one person/team can handle, such as 25-50 doors. Similar practices are used in other fields such as door-to-door fundraising and census data collection.

Within the navigation menu, Turfcutter is located within Campaigns near the bottom of the dropdown.


## Map
The map contains information about geographical locations. While showing voters, it'll put a pin on each voter. While in volunteer view, previous turfs of the volunteer will be shown to give context to the turfmaker.

~~While in heat-map view the markers will change color to correspond to voter information.~~
Somehow, the heatmap.js disappeared. Let's put a pin in this functionality.

What type of information this will be can be set in an options menu. For example, you could query the data and ask "what voters have not been canvassed in the last two weeks?" The color of markers will change accordingly.


## Future Work
This turfcutter, though working for the features it has currently, is by no means complete. In the future hopefully an autocutting feature will be added as well as google-maps intergration allowing for visualization of turf-routes and walking time estimates. 

## Technologies 

For the creation of the user interface several different technologies will be used in addition to CiviCRM.
Leaflet, a javascript interactive maps libary, will be heavily utilized for displaying and interacting with maps. "https://leafletjs.com/"

Turf.js is probably the largest part of the application in terms of hard drive real-estate. It allows for logic to be performed on the mapping data. In particular, it is useful for figuring out who is inside of a polygon. "http://turfjs.org/"

Leaflet.markercluster is used in order to improve the performance whenever there is a large amount of markers being loaded. "https://github.com/Leaflet/Leaflet.markercluster"

leaflet-sidebar-v2 is used in order to create the left and rightmost sidebars. "https://github.com/nickpeihl/leaflet-sidebar-v2"

Though technically not an extension, it is worth noting that leaflet has a layers feature which will be used for implementing the voter view and volunteer view. "https://leafletjs.com/examples/layers-control/"

Leaflet draw is used to draw polygons. "https://github.com/Leaflet/Leaflet.draw"

Leaflet fullscreen is an extension which allows for fullscreen usage of the turfcutter. "https://github.com/Leaflet/Leaflet.fullscreen"


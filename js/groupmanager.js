function turf_export_button() {
//check and see if parent is built
//console.log("checking to see if parent is built");
var parent_group_id = turf_parent_checker();
//console.log(parent_group_id);
if (parent_group_id == null) {
  //console.log("Parent group was null, building");
            build_parent_group();
            parent_group_id = turf_parent_checker();
            //console.log("parent group built");
            //console.log(parent_group_id);
}
console.log("Beginning turf processing");
process_the_turfs(parent_group_id);

      
}


function process_the_turfs(parent_group_id){

var a = [];
myData = document.getElementById("table_turf_table").tBodies[0].rows;
for (var i = 1; i < myData.length; i++) {
  el = Number(myData[i].children[3].innerText);
  turfname = myData[i].children[1].innerText;
  a.push({turfname, el});
  }
// At this point, a is an array of leaflet IDs to process.
console.log(a);
console.log("Beginning loop");
for (let counter = 0; counter < a.length; ++counter) {
var turftoprocess = a[counter]
console.log("doing the loop with...");
console.log(a[counter]);
console.log(turftoprocess['turfname']);
 // First we send the voters over to the groupcontact machine
upload_the_contacts_turfed(turftoprocess['turfname'],turftoprocess['el'],parent_group_id);

// Then, we'll delete them from the table...
 var row = document.getElementById(turftoprocess['el']);
 row.parentNode.removeChild(row);

// Finally, we delete the shape itself. And ideally hide the contacts too.
drawnItems.removeLayer(turftoprocess['el']);
}
}

function upload_the_contacts_turfed(name,leafletid,parent_group_id) {
//make this whole thing
// First, make a group below parent to serve as the collection for all of these turfs. We'll call it an auntie.
build_auntie_group(parent_group_id);
// Figure out Javascript.
// once that one's done, then...
var auntie_id = auntie_checker(parent_group_id);


 //Get the voters in that turf
console.log("heres where id put an upload script");
//voterIDsWithinPolygon = find_voters_within_clickedturf(leafletid);
}

/*
EVERYTHING BELOW THIS LINE WORKS FINE. DONT TOUCH IT.
*/
function auntie_checker(parent_group_id) {
var auntie_name = document.getElementById("auntie_name");
CRM.api3('Group', 'get', {
  "return": ["id"],
  "name": auntie_name,
  "parents": parent_group_id,
  "options": {"limit":1}
}).done(function (result) {
        // do something
        for (var i = 0; i < result.values.length; i++) {
            auntie_group_id = result.values[i].id;
        }
    });

    return auntie_group_id;
}

function build_auntie_group(parent_group_id) {
var auntie_name = document.getElementById("auntie_name");
CRM.api3('Group', 'create', {
  "title": auntie_name,
  "name": auntie_name,
  "parents": parent_group_id
});
}


function build_parent_group() {
CRM.api3('Group', 'create', {
  "title": "Turfcutter",
  "name": "Turfcutter"
});
}


function turf_parent_checker() {
 var parent_group_id = null;
CRM.api3('Group', 'get', {
  "sequential": 1,
  "return": ["id"],
  "name": "Turfcutter",
  "parents": {"IS NULL":1},
  "options": {"limit":1}
}).done(function (result) {
        // do something
        for (var i = 0; i < result.values.length; i++) {
            parent_group_id = result.values[i].id;
        }
    });

    return parent_group_id;
}

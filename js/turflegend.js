function populate_turf_list(leafletsentin, headcount) {
    var leafletid = leafletsentin;
    //var layercolor = drawnItems.getLayer(leafletid).options.color;

    // Find a <table> element with id="myTable":
    var table = document.getElementById("table_turf_table");
    // var tbody = table.tBodies[0];
    //table.deleteRow(shapecount);

    var row = document.getElementById(leafletid);
    if (row == null) {
        //  console.log("isnull");
    } else {
        console.log(row);
        row.parentNode.removeChild(row);
    }
    var a = [];
    myData = document.getElementById("table_turf_table").tBodies[0].rows;
    for (var i = 1; i < myData.length; i++) {
        el = Number(myData[i].children[4].innerText);
        a.push(el)
    }
    var count = 35;
    var missing = [];
    for (let i = 1; i <= count; i++) {
        if (a.indexOf(i) === -1) {
            missing.push(i)
        }
    }
    //console.log(missing);
    //console.log(a);
    var indexcount = Math.min(...missing);
    //console.log(indexcount);

    var colorarray = ["#XXXXXX", "#0000FF", "#008000", "#8B008B", "#FF0000", "#FFD700", "#008080", "#D2691E", "#6A5ACD", "#00FFFF", "#FF1493", "#7FFF00", "#6495ED", "#E9967A", "#808000", "#FF8C00", "#00FF00", "#483D8B", "#FFFF00", "#9ACD32", "#FFC0CB", "#800000", "#A0522D", "#7FFFD4", "#4B0082", "#DB7093", "#000080", "#00FF7F", "#DDA0DD", "#8FBC8F", "#F08080", "#F0E68C", "#FF4500", "#041690", "#32CD32", "#9932CC"];

    var layercolor = colorarray[indexcount];
    drawnItems.getLayer(leafletid).setStyle({
        color: layercolor
    });

    //console.log(indexcount);


    //var indexcount = colorarray.indexOf(layercolor);


    //    var tbodyRowCount = (table.rows.length);

    // Create an empty <tr> element and add it to the 1st position of the table:
    var row = table.insertRow(indexcount);
    row.id = leafletid;

    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);

    cell4.style = "display:none;";
    cell5.style = "display:none;";

    cell1.id = "colornum";
    cell2.id = "turfnum";
    cell3.id = "headcount";
    cell4.id = "leafletid";
    cell5.id = "indexnum";

    // Add some text to the new cells:
    cell1.innerHTML = layercolor;
    cell2.innerHTML = "Turf " + indexcount;
    cell3.innerHTML = headcount;
    cell4.innerHTML = leafletid;
    cell5.innerHTML = indexcount; //tbodyRowCount;


}

function turf_list_delete(leafletid) {
    // var table = document.getElementById("table_turf_table");
    console.log(leafletid);
    var row = document.getElementById(leafletid);
    console.log(row);
    row.parentNode.removeChild(row);
    console.log("shape deleted");
}


<!-- Nav tabs -->
        <div id="sidebar-right" class="leaflet-sidebar">
            <div class="leaflet-sidebar-tabs">
                <ul role="tablist">
                    <li><a href="#righthome" role="tab"><i class="fa fa-bars active"><p class="icon-center-rightsidebar">{ts}Turfs{/ts}</p></i></a></li>
                </ul>
            </div>
            <!-- Tab panes -->
            <div class="leaflet-sidebar-content">
                <div class="leaflet-sidebar-pane" id="righthome">
                    <!-- Content of the right home -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}TURF LEGEND{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-right"></i></span>
                    </h1>
                     <p class="lorem">{ts}Turfs and their assignments{/ts}</p>
<div class="turf_export_button"><button name="turf_export_button" class="turf_export_button" type="button" onclick="turf_export_button()">Export Turfs</button><input type="text" id="auntie_name" placeholder="Turf Group Name?" maxlength="20"/></div><br />
                    <table class="table_turf_table" id="table_turf_table">
                    <tr id="turf_table_header"><th id="turf_table_color">color</th><th id="turf_table_name">name</th><th id=turf_table_headcount"">headcount</th><th style="display:none;" id="turf_table_leafletid"></th><th style="display:none;" id="turf_table_index"></th></tr>
                        </table>
                      
                        <script>
        </script>
        
                </div>
            </div>

        </div>

<!DOCTYPE html> 
<html>
    <head>
        <title>CiviCRM Turfcutter</title>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        {literal}
        <style>
            html,  body {
                 padding: 0;
                 margin: 0;
             }

             html, body, #map {
                 height: 500px;
            <!--  width: 800px;-->
                 font: 10pt "Helvetica Neue", Arial, Helvetica, sans-serif;
             }

             .lorem {
                 font-style: italic;
                 text-align: justify;
                 color: #AAA;
             }
             ul,
             .block ul,
             ol {
                margin: 0 0 0 0 !important;
                padding : 0 0 0 0 !important;
             }

             ol li,
             ul li,
             ul.menu li,
             .item-list ul li,
             li.leaf {
                margin: 0 0 0 0  !important; /* LTR */
                padding-bottom: 0 !important;
             }

             h2,
             #center h1{
                line-height: 180% !important;
                font-size: 160% !important;
             }
            </style>
            {/literal}
    </head>
    <body>
      {include file='CRM/Turfcutter/Page/LeftSidebar.tpl'}
      {include file='CRM/Turfcutter/Page/RightSidebar.tpl'}
      
      <div id="map"></div>

       {literal}
      <script>
             var voters =  L.markerClusterGroup({maxClusterRadius:function(e){return maxClusterRadius(e)},showCoverageOnHover:false,
                                                 disableClusteringAtZoom:13
                                                 //Disable clustering at zoom of 1 disables it bigly. Set it to like 13 to reenable
                                                });
         var volunteers = get_volunteer_markers_from_civi();  //get_voters.js line 35

             var popupGroup = L.layerGroup([]);

             //This function defines how much clustering will occur. Note that at all zooms 18 and greater than clustering is disabled
             function maxClusterRadius(zoomlevel){
               var startingValue = 10;
               var endValue = 0;
               var maxZoomLevel = 20;
               return startingValue + (startingValue-endValue)/maxZoomLevel * zoomlevel;
             }

         var defaultZoom = 12;

             var map = L.map('map', {
               fullscreenControl: true,
               center: map_initial_center(), //Note this function is located inside of volunteerinfo.js
           zoom: defaultZoom,
           maxZoom : 20,
             layers: [voters, googleStreets]
             });

         /*
          All of the base maps are defined within the tilelayer.js file
         
         var baseMaps = {
             "Google Street map": googleStreets,
             "Open Street Maps": openStreetMaps,
             "Open Street Maps BW": openStreetMaps_BW,
             "Google Hybrid Map": googleHybrid,
             "Google Satellite Map": googleSat,
             "Google Terrain Map": googleTerrain,
             "ArcGis": arcGis,
             "Open Topological Map" : openTopoMap,
             "Open Roads Maps" : OpenMapSurfer_Roads,
             "Open Roads Greyscale" : OpenMapSurfer_Grayscale,
             "Stamen Toner" : Stamen_Toner,
             "Stamen Tonerlite" : Stamen_TonerLite,
             "Esri World Street Map" : Esri_WorldStreetMap,
             "Esri National Geographic World Map" : Esri_NatGeoWorldMap,
             "Carto" : CartoDB_Voyager,
             "HikeBike" : HikeBike_HikeBike,
             "Wikimedia" : Wikimedia,
             "Open PT Map" : OpenPtMap,
             "Open Railway Map" : OpenRailwayMap
         };
         */
var baseMaps = {
             "Google Street map": googleStreets,
             "Open Street Maps": openStreetMaps,
             "Open Street Maps BW": openStreetMaps_BW,
             "Google Hybrid Map": googleHybrid,
             "Google Satellite Map": googleSat,
         };

         var overlayMaps = {
             "Voter Map": voters,
             "Volunteer Map": volunteers
         };

         map.addControl(new L.Control.Scale());

         L.control.layers(baseMaps, overlayMaps).addTo(map);

         var drawnItems = new L.FeatureGroup();

         var options = {
             position: 'topleft',
             editable : false,
             draw: {
                 polyline: false,
                 polygon: {
                     allowIntersection: false, // Restricts shapes to simple polygons
                     drawError: {
                         color: '#f4355f', // Color the shape will turn when intersects
                         message: '<strong>Oh snap!<strong> Your shape cannot intersect itself.' // Message that will show when intersect
                     },
                     shapeOptions: {
                         color: '#f4355f'
                     }
                 },
                 marker : false,
                 rectangle: false, circle: false,
                 //circle: true, rectangle: {shapeOptions: { clickable: false }},
                 circlemarker : false,
             },
             edit: { featureGroup: drawnItems }
         };
         map.addLayer(popupGroup);
         map.addLayer(drawnItems);

         var drawControl = new L.Control.Draw(options);
         map.addControl(drawControl);

         drawControlEdit = new L.Control.Draw(options);

         map.on('draw:created', function(e) {
             var type = e.layerType,
                 layer = e.layer;
              drawnItems.addLayer(layer);

              var shapecount = drawnItems.getLayers().length;
              /*
              var shapecolor = "";
              switch (shapecount){
                  case 1: shapecolor = "#0000FF"; break;
case 2: shapecolor = "#008000"; break;
case 3: shapecolor = "#8B008B"; break;
case 4: shapecolor = "#FF0000"; break;
case 5: shapecolor = "#FFD700"; break;
case 6: shapecolor = "#008080"; break;
case 7: shapecolor = "#D2691E"; break;
case 8: shapecolor = "#6A5ACD"; break;
case 9: shapecolor = "#00FFFF"; break;
case 10: shapecolor = "#FF1493"; break;
case 11: shapecolor = "#7FFF00"; break;
case 12: shapecolor = "#6495ED"; break;
case 13: shapecolor = "#E9967A"; break;
case 14: shapecolor = "#808000"; break;
case 15: shapecolor = "#FF8C00"; break;
case 16: shapecolor = "#00FF00"; break;
case 17: shapecolor = "#483D8B"; break;
case 18: shapecolor = "#FFFF00"; break;
case 19: shapecolor = "#9ACD32"; break;
case 20: shapecolor = "#FFC0CB"; break;
case 21: shapecolor = "#800000"; break;
case 22: shapecolor = "#A0522D"; break;
case 23: shapecolor = "#7FFFD4"; break;
case 24: shapecolor = "#4B0082"; break;
case 25: shapecolor = "#DB7093"; break;
case 26: shapecolor = "#000080"; break;
case 27: shapecolor = "#00FF7F"; break;
case 28: shapecolor = "#DDA0DD"; break;
case 29: shapecolor = "#8FBC8F"; break;
case 30: shapecolor = "#F08080"; break;
case 31: shapecolor = "#F0E68C"; break;
case 32: shapecolor = "#FF4500"; break;
case 33: shapecolor = "#041690"; break;
case 34: shapecolor = "#32CD32"; break;
case 35: shapecolor = "#9932CC"; break;
                  default:
                  shapecolor = "white";
              }
              
              layer.setStyle({color: shapecolor});

              //console.log("below is the layer._leaflet_id");
              //console.log(layer._leaflet_id);
              */
         });

         map.on('draw:deleted', function (e) {
        var deletedLayers = e.layers._layers;
        //console.log(deletedLayers);
        for (var layer in deletedLayers) {
           console.log(deletedLayers[layer]);
           turf_list_delete(deletedLayers[layer]._leaflet_id);
        }
             drawControlEdit.remove();
             drawControl.addTo(map);

         });


         map.on(L.Draw.Event.CREATED, function (e) {
             var type = e.layerType,
                 layer = e.layer;
                 //console.log("below is the layer._leaflet_id");
                 //console.log(layer._leaflet_id);
                 
                    /*   start test code   */
                 
                     var editlatlng = layer.getBounds().getCenter();
                     var voterIDsWithinPolygon = new Set([]);
                     voterIDsWithinPolygon = find_voters_within_clickedturf(layer._leaflet_id);
                     /*
                     var popupcontentPolygon = get_popupcontentPolygon(layer.getBounds().getCenter(),voterIDsWithinPolygon);
                     var newlayer = L.popup().setLatLng([editlatlng.lat, editlatlng.lng]).setContent(popupcontentPolygon);
                     popupGroup.addLayer(newlayer);
                     newlayer.openPopup();
                     */
                     populate_turf_list(layer._leaflet_id,voterIDsWithinPolygon.size);

                     /*   end test code    */
                 layer.on('click', function () {
                     var editlatlng = layer.getBounds().getCenter();
                     var voterIDsWithinPolygon = new Set([]);
                     voterIDsWithinPolygon = find_voters_within_clickedturf(layer._leaflet_id);
                     /*
                     var popupcontentPolygon = get_popupcontentPolygon(layer.getBounds().getCenter(),voterIDsWithinPolygon);
                     var newlayer = L.popup().setLatLng([editlatlng.lat, editlatlng.lng]).setContent(popupcontentPolygon);
                     popupGroup.addLayer(newlayer);
                     newlayer.openPopup();
                     */
                     populate_turf_list(layer._leaflet_id,voterIDsWithinPolygon.size);
                     
});
         });

         var leftsidebar = L.control.sidebar({ container: 'sidebar-left', position : 'left' })
                            .addTo(map)
                            .close('lefthome');
      
        var rightsidebar = L.control.sidebar({ container: 'sidebar-right', position : 'right' })
                            .addTo(map)
                            .close('righthome');
                         

        //generate_voter_group_list
        var legend = L.control({position: 'topright'});
             legend.onAdd = function (map) {
               var div = L.DomUtil.create('div', 'info legend');
               div.innerHTML = generate_voter_group_list(1);
               div.setAttribute("id","info_legend");
        return div;
             };
             legend.addTo(map);

/*
             //generate turflegend
             var turflegend = L.control({position: 'bottomright'});
             turflegend.onAdd = function (map) {
               var div2 = L.DomUtil.create('div', 'info legend');
               //div2.innerHTML = generate_voter_group_list234();
               div2.innerHTML = generate_turflegend();
               //div.innerHTML = "<h1> BIG TEST WORDS </H1>";
               div2.setAttribute("id","info_legend");
        return div2;
             };
             turflegend.addTo(map);

             */

        </script>
        {/literal}
    </body>
</html>

<div id="sidebar-left" class="leaflet-sidebar collapsed">
            <!-- Nav tabs -->
            <div class="leaflet-sidebar-tabs">
                <ul role="tablist">
                    <li><a href="#lefthome" role="tab" style="width:40px;height:40px;" ><i class="fa fa-bars" style="padding-top:5px"><p class="icon-center">Home</p></i></a></li>
                    <li><a href="#lefttutorial" role="tab" style="width:40px;height:40px;" ><i class="fa fa-graduation-cap" style="padding-top:5px"><p class="icon-center">Learn</p></i></a></li>
                    <li><a href="#leftdocumentation" role="tab" style="width:40px;height:40px;" ><i class="fa fa-book" style="padding-top:5px"><p class="icon-center">Docs</p></i></a></li>
					<!-- <li><a href="#leftprofile" role="tab" style="width:40px;height:40px;"><i class="fa fa-user" style="padding-top:5px" ><p class="icon-center">{ts}Vollies{/ts}</p></i></a></li> -->
                </ul>
                <!-- Link to gitlab of project 
                <ul role="tablist">
                    <li><a href="https://gitlab.com/cully/turfcutter"><i class="fa fa-gitlab"><p  class="icon-center">Gitlab</p></i></a></li>
                </ul>-->
            </div>

            <!-- Tab panes -->
            <div class="leaflet-sidebar-content">
                <div class="leaflet-sidebar-pane" id="lefthome">
                    <!-- Content of left home goes here -->
                    <h1 class="leaflet-sidebar-header">
                        HOME
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <h1 class="lorem"><center>{ts}CiviCRM Turfcutter{/ts}</center></h1>
                    <h3>{ts}What is turfcutting?{/ts}</h3>
                    <p>{ts}Turfcutting is when a large group of contacts is divided into smaller lists more suitable for door-to-door activities. For example, in political campaigns each person who canvasses must receive a group of people to canvass, and a walklist of 50 doors is much more managable than giving them the entire precinct!{/ts}</p>
                    
                    <h4>{ts}Click the graduation cap to the left for a tutorial.{/ts}</h4>
                </div>
                <div class="leaflet-sidebar-pane" id="lefttutorial">
                    <!-- Content of left tutorial goes here -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}TUTORIAL{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <p class="lorem">{ts}This application is a turfcutter, the purpose of which is to assist in creating groups of individuals from their locations.{/ts}</p>
                    <h2>{ts}How to create a turf{/ts}</h2>
                    <p class="lorem">{ts}Step 1: Choose a shape which you want to use to create your turf from the left shape menu.{/ts}</p>
                    <p class="lorem">{ts}Step 2: Draw a shape around some markers! This represents a turf. A popup will appear.{/ts}</p>
                    <p class="lorem">{ts}Step 3: This popup has several options. After selecting the parameters you want, decide whether you want to make an additional shape for this turf, save the voters as a group, or assign the turf to a volunteer.{/ts}</p>
                    <p class="lorem">{ts}The base map that is used can be changed from the menu icon in the top right of the map. There are currectly seventeen options.{/ts}</p>
                </div>
                <div class="leaflet-sidebar-pane" id="leftdocumentation">
                    <!-- Content of left documentation goes here -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}DOCUMENTATION{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <p class="lorem">{ts}This map is based upon <a href="https://leafletjs.com/">leaflet</a>, an open source javascript maps library.{/ts}</p>
                    <p class="lorem">{ts}Within leaflet each object (marker, polygon, rectangle etc) is stored as a "layer". A collection of layers is a "layer group".{/ts}</p>
                    <p class="lorem">{ts}There are three layer groups that are used for the turfcutter. A voter group which stores voter markers, a volunteer group, which stores volunteer homes and work, and a heat map group, which allows for statistical queries on voter and volunteer data.{/ts}</p>
                    <p class="lorem">{ts}CiviCRM is extended with several new API calls. The SurveyRespondant Entity has a create option and the Survey API is extended and now allows to alllow for adding voters to surveys.{/ts}</p>
                </div>
                <div class="leaflet-sidebar-pane" id="leftaboutus">
                    <!-- Content of left about us goes here -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}ABOUT US{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <p class="lorem">{ts}Much of the software was designed by Alex Sludds (alexsludds@protonmail.com){/ts}</p>
                    <p class="lorem">{ts}Special thanks to Monish, Joe Murray, and Joe McLaughlin for their help and insight.{/ts}</p>
                    <p class="lorem">{ts}As of early 2022, this project was stale. I forked it and here we are.{/ts}</p>
                </div>
                <div class="leaflet-sidebar-pane" id="leftprofile">
                    <!-- Content of the volunteer information -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}VOLUNTEER INFORMATION{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <p class="lorem">{ts}Here is the list of volunteers and their status for turf assignment.{/ts}</p>
                    <nav>
                        <ul id="volunteer_info_volunteer_list" class="volunteer_info_volunteer_list">
                        </ul>
                        <script>populate_volunteer_list()</script>
                    </nav>
                </div>
            </div>
        </div>
